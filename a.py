import pygame
import random
import time
import json
pygame.init()
pygame.font.init()
clock = pygame.time.Clock()

with open('config.json') as config_file:
    data = json.load(config_file)

w = data['width']
h = data['height']
q_1 = data['q_1']
q_2 = data['q_2']
q_3 = data['q_3']
q_4 = data['q_4']
q_5 = data['q_5']

win = pygame.display.set_mode((w, h))

red = (q_2, q_1, q_1)

img = pygame.image.load('download.png')
smallfont = pygame.font.SysFont("Comic Sans MS", 30)


def disp():
    global f_1
    global f_2
    global k_3
    global k_1
    global k_2
    global p_1
    global p_2
    if s_1 == 60 or lst[0] == 1:
        if f_1 == 0:
            win.fill((255, 255, 255))
            f_1 = 1
            if lst[0] == 1:
                text_1 = smallfont.render("PLAYER1 DIED!!!", True, (0, 0, 0))
                win.blit(text_1, [250, 300])
            text_1 = smallfont.render("SCORE: " + str(s_1), True, (0, 0, 0))
            win.blit(text_1, [250, 340])
            pygame.display.update()
            time.sleep(2)
    if s_2 == 60 or lst[1] == 1:
        if f_2 == 0:
            win.fill((255, 255, 255))
            f_2 = 1
            if lst[1] == 1:
                text_1 = smallfont.render("PLAYER2 DIED!!!", True, (0, 0, 0))
                win.blit(text_1, [250, 300])
            text_1 = smallfont.render("SCORE: " + str(s_2), True, (0, 0, 0))
            win.blit(text_1, [250, 340])
            pygame.display.update()
            time.sleep(2)
    if ((lst[0] == 1 and lst[1] == 1) or (s_1 == 60 and s_2 == 60)):
        if k_1 == 0:
            k_1 == 1
            win.fill((255, 255, 255))
            if s_1 == s_2:
                if p_1 < p_2:
                    text_2 = smallfont.render("PLAYER1 WON!!!", True, 0)
                if p_1 > p_2:
                    text_2 = smallfont.render("PLAYER2 WON!!!", True, 0)
                if p_1 == p_2:
                    text_2 = smallfont.render("BOTH WON!!!", True, 0)
            if s_1 < s_2:
                text_2 = smallfont.render("PLAYER2 WON!!!", True, 0)
            if s_1 > s_2:
                text_2 = smallfont.render("PLAYER1 WON!!!", True, (0, 0, 0))
            win.blit(text_2, [250, 300])
            pygame.display.update()
            time.sleep(2)

    elif s_1 == 60 and lst[1] == 1 and k_2 == 0:
        win.fill((255, 255, 255))
        k_2 = 1
        text_2 = smallfont.render("PLAYER1 WON!!!", True, (0, 0, 0))
        win.blit(text_2, [250, 300])
        pygame.display.update()
        time.sleep(2)

    elif s_2 == 60 and lst[0] == 1 and k_3 == 0:
        win.fill((255, 255, 255))
        k_3 = 1
        text_2 = smallfont.render("PLAYER2 WON!!!", True, (0, 0, 0))
        win.blit(text_2, [250, 300])
        pygame.display.update()
        time.sleep(2)


def get_score1():
    h_score = 0
    try:
        h_score_file = open("score.txt", "r")
        h_score = int(h_score_file.read())
        print(h_score)
    except IOError:
        # Error reading file, no high score
        print("There is no high score yet.")
    except ValueError:
        # There's a file there, but we don't understand the number.
        print("I'm confused. Starting with no high score.")
    return h_score


def s_hscore(n_hscore):
    try:
        h_score_file = open("score.txt", "w")
        h_score_file.write(str(n_hscore))
        h_score_file.close()
    except IOError:
        # Hm, can't write it.
        print("Unable to save the score.")


def detectCollisions(x1, y1, w1, h1, x2, y2, w2, h2):

    if (x2 + w2 >= x1 >= x2 and y2 + h2 >= y1 > y2):
        return True
    elif (x2 + w2 >= x1 + w1 >= x2 and y2 + h2 >= y1 >= y2):
        return True
    elif (x2 + w2 >= x1 >= x2 and y2 + h2 >= y1 + h1 > y2):
        return True
    elif (x2 + w2 >= x1 + w1 >= x2 and y2 + h2 >= y1 + h1 >= y2):
        return True
    else:
        return False


class play(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height


class speed(object):
    def __init__(self, vel):
        self.vel = vel


def score(score):
    text = smallfont.render("Score: " + str(score), True, (0, 0, 0))
    win.blit(text, [0, 0])


collision1 = False
collision2 = False

m1 = speed(10)
m2 = speed(10)

man1 = pygame.Rect(325, 665, 40, 30)
man2 = pygame.Rect(325, 5, 40, 30)

obs1 = play(325, 325, 40, 30)
obs2 = play(0, 86, 40, 30)
obs3 = play(220, 700 - 86, 40, 30)

p_1 = 0
p_2 = 0

f_1 = 0
f_2 = 0

s_1 = 0
s_2 = 0

a_1 = 0
a_2 = 0

lst = [a_1, a_2]
r_1 = 0
r_2 = 0
r_3 = 0
r_4 = 0
r_5 = 0
r_6 = 0
r_7 = 0
r_8 = 0
r_9 = 0

r_11 = 0
r_12 = 0
r_13 = 0
r_14 = 0
r_15 = 0
r_16 = 0
r_17 = 0
r_18 = 0
r_19 = 0

k_1 = 0
k_2 = 0
k_3 = 0

lol = 0
end_time_1 = 0
start_time_1 = 0
end_time_2 = 0
start_time_2 = 0

walls = [
    pygame.Rect(280, 0, 40, 40),
    pygame.Rect(400, 132, 40, 40),
    pygame.Rect(110, 264, 40, 40),
    pygame.Rect(440, 396, 40, 40),
    pygame.Rect(300, 528, 40, 40),
    pygame.Rect(120, 660, 40, 40),
]
p = 0
q = 0
run = True
while run:
    pygame.time.delay(100)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    if lst[0] == 1 and s_2 == 60:
        m2.vel += 1
    if lst[1] == 1 and s_1 == 60:
        m1.vel += 1
    if s_1 == 60 and s_2 == 60:
        m2.vel += 1
        m1.vel += 1
    if (lst[0] == 1 and lst[1] == 1) or (lst[0] == 1 and s_2 == 60) or (
            lst[1] == 1 and s_1 == 60) or (s_1 == 60 and s_2 == 60):

        lst[0] = 0
        lst[1] = 0

        f_1 = 0
        f_2 = 0

        p = 0
        q = 0

        s_1 = 0
        s_2 = 0

        r_1 = 0
        r_2 = 0
        r_3 = 0
        r_4 = 0
        r_5 = 0
        r_6 = 0
        r_7 = 0
        r_8 = 0
        r_9 = 0

        r_11 = 0
        r_12 = 0
        r_13 = 0
        r_14 = 0
        r_15 = 0
        r_16 = 0
        r_17 = 0
        r_18 = 0
        r_19 = 0

        k_1 = 0
        k_2 = 0
        k_3 = 0

        collision1 = False
        collision2 = False

        man1 = pygame.Rect(325, 665, 40, 30)
        man2 = pygame.Rect(325, 5, 40, 30)

    keys = pygame.key.get_pressed()

    if lst[0] == 0 and s_1 != 60:
        if keys[pygame.K_LEFT] and man1.x > 5:
            man1.x -= 5
        if keys[pygame.K_RIGHT] and man1.x < 700 - man1.width - 5:
            man1.x += 5
        if keys[pygame.K_UP] and man1.y > 5:
            man1.y -= 66
        if keys[pygame.K_DOWN] and man1.y < 700 - man1.height - 5:
            man1.y += 66
        vel = m1.vel
        start_time_1 = time.time()

    if lst[1] == 0 and s_2 != 60 and (man1.y == 5 or lst[0] == 1):
        if keys[ord('a')] and man2.x > 5:
            man2.x -= 5
        if keys[ord('d')] and man2.x < 700 - man2.width - 5:
            man2.x += 5
        if keys[ord('s')] and man2.y < 700 - man2.height - 5:
            man2.y += 66
        if keys[ord('w')] and man2.y > 5:
            man2.y -= 66
        vel = m2.vel
        start_time_2 = time.time()

    if obs1.x < 700 - vel - obs1.width:
        obs1.x += vel
    else:
        obs1.x = vel

    if obs2.x < 700 - vel - obs2.width:
        obs2.x += vel
    else:
        obs2.x = vel

    if obs3.x < 700 - vel - obs3.width:
        obs3.x += vel
    else:
        obs3.x = vel

    win.fill((0, 255, 255))

    for wall in walls:
        if man1.colliderect(wall):
            collision1 = True
    for wall in walls:
        if man2.colliderect(wall):
            collision2 = True

    if collision1:
        lst[0] = 1
    if collision2:
        lst[1] = 1

    if man1.y <= 660 and lst[0] == 0 and r_1 == 0:
        s_1 += 5
        r_1 = 1
    elif man1.y <= 528 and lst[0] == 0 and man1.y >= 436:
        if r_2 == 0:
            r_2 = 1
            s_1 += 5
    elif man1.y <= 396 and lst[0] == 0 and man1.y >= 304:
        if r_3 == 0:
            r_3 = 1
            s_1 += 5
    elif man1.y <= 264 and lst[0] == 0 and man1.y >= 172:
        if r_4 == 0:
            r_4 = 1
            s_1 += 5
    elif man1.y <= 132 and lst[0] == 0 and man1.y >= 40:
        if r_5 == 0:
            r_5 = 1
            s_1 += 5
    elif man1.y <= 40 and lst[0] == 0 and man1.y >= 0:
        if r_9 == 0:
            r_9 = 1
            s_1 += 5

    if man1.y < 295:
        if r_6 == 0:
            r_6 = 1
            s_1 += 10
    if man1.y < 56:
        if r_7 == 0:
            r_7 = 1
            s_1 += 10
    if man1.y < 583:
        if r_8 == 0:
            r_8 = 1
            s_1 += 10

    if lst[0] == 0:
        collision1 = detectCollisions(
            man1.x, man1.y, man1.width, man1.height, obs1.x, obs1.y,
            obs1.width, obs1.height) or detectCollisions(
                man1.x, man1.y, man1.width, man1.height, obs2.x, obs2.y,
                obs2.width, obs2.height) or detectCollisions(
                    man1.x, man1.y, man1.width, man1.height, obs3.x, obs3.y,
                    obs3.width, obs3.height)
    if lst[1] == 0:
        collision2 = detectCollisions(
            man2.x, man2.y, man2.width, man2.height, obs1.x, obs1.y,
            obs1.width, obs1.height) or detectCollisions(
                man2.x, man2.y, man2.width, man2.height, obs2.x, obs2.y,
                obs2.width, obs2.height) or detectCollisions(
                    man2.x, man2.y, man2.width, man2.height, obs3.x, obs3.y,
                    obs3.width, obs3.height)
    if collision1:
        lst[0] = 1
    if collision2:
        lst[1] = 1

    if man2.y >= 40 and lst[1] == 0 and r_11 == 0:
        s_2 += 5
        r_11 = 1
    elif man2.y <= 660 and lst[1] == 0 and man2.y >= 568:
        if r_12 == 0:
            r_12 = 1
            s_2 += 5
    elif man2.y >= 700 - 264 and lst[1] == 0 and man2.y <= 700 - 172:
        if r_13 == 0:
            r_13 = 1
            s_2 += 5
    elif man2.y >= 700 - 396 and lst[1] == 0 and man2.y <= 700 - 304:
        if r_14 == 0:
            r_14 = 1
            s_2 += 5
    elif man2.y >= 700 - 528 and lst[1] == 0 and man2.y <= 700 - 436:
        if r_15 == 0:
            r_15 = 1
            s_2 += 5
    elif man2.y <= 700 and lst[1] == 0 and man2.y >= 660:
        if r_19 == 0:
            r_19 = 1
            s_2 += 5
    if man2.y > 700 + 30 - 86:
        if r_16 == 0:
            r_16 = 1
            s_2 += 10
    elif man2.y > 355:
        if r_17 == 0:
            r_17 = 1
            s_2 += 10
    elif man2.y > 116:
        if r_18 == 0:
            r_18 = 1
            s_2 += 10
    pygame.time.delay(10)
    if lst[0] == 1 or s_1 == 60:
        if p == 0:
            end_time_1 = time.time()
            p_1 = (end_time_1 - start_time_1) % 60
            p = 1

    if lst[1] == 1 or s_2 == 60:
        if q == 0:
            end_time_2 = time.perf_counter()
            p_2 = (end_time_2 - end_time_1) % 60
            q = 1

    disp()
    pygame.draw.rect(win, (128, 0, 0), (0, 0, 700, 40))
    pygame.draw.rect(win, (128, 0, 0), (0, 132, 700, 40))
    pygame.draw.rect(win, (128, 0, 0), (0, 264, 700, 40))
    pygame.draw.rect(win, (128, 0, 0), (0, 396, 700, 40))
    pygame.draw.rect(win, (128, 0, 0), (0, 528, 700, 40))
    pygame.draw.rect(win, (128, 0, 0), (0, 660, 700, 40))

    win.blit(pygame.transform.scale(img, (40, 40)), (280, 0))
    win.blit(pygame.transform.scale(img, (40, 40)), (400, 132))
    win.blit(pygame.transform.scale(img, (40, 40)), (110, 264))
    win.blit(pygame.transform.scale(img, (40, 40)), (440, 396))
    win.blit(pygame.transform.scale(img, (40, 40)), (300, 528))
    win.blit(pygame.transform.scale(img, (40, 40)), (120, 660))

    if lst[0] == 0 and s_1 != 60:
        score(s_1)
    elif lst[0] == 1 or s_1 == 60:
        score(s_2)

    if lst[1] == 0:
        pygame.draw.rect(win, q_2, man2)
    if lst[0] == 0:
        pygame.draw.rect(win, q_1, man1)

    pygame.draw.rect(win, 128, (obs1.x, obs1.y, obs1.width, obs1.height))
    pygame.draw.rect(win, 128, (obs2.x, obs2.y, obs2.width, obs2.height))
    pygame.draw.rect(win, 128, (obs3.x, obs3.y, obs3.width, obs3.height))
    pygame.display.update()

pygame.quit()
